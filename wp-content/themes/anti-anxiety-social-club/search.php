<?php

global $post;
global $wp_query;

$search_value = get_search_query();
$page = get_query_var('paged');

use Backstage\Models\PostFactory;
use ChildTheme\Components\PostActionCard\PostActionCardView; ?>

<div class="content-section">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <h1 class="heading heading--large">Search for: </h1>
                </div>
            </div>
        </div>
        <div class="content-row row">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <?= get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section">
    <div class="container content-section__container">
        <?php if ($wp_query->have_posts()): ?>
            <?php while($wp_query->have_posts()): the_post(); ?>
                <div class="content-row row content-row--mb-half">
                    <div class="content-column col-md-12">
                        <div class="content-column__inner">
                            <?= new PostActionCardView(PostFactory::create($post)); ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php else: ?>
            <div class="content-row row content-row--mb-half">
                <div class="content-column col-md-12">
                    <div class="content-column__inner">
                        <strong class="heading heading--large">There are no results for that search query.</strong>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="content-section content-section--has-bg">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <?php the_posts_pagination(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
