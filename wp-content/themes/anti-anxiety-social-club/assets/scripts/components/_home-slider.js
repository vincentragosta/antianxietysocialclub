addAction(INIT, function () {
    var $mainSlider = $('.home-slider__main');
    var $navContainer = $('.home-slider__nav-container');
    var $navSlider = $navContainer.find('.home-slider__nav');

    $mainSlider.owlCarousel({
        center: true,
        slideSpeed: 2000,
        autoplayTimeout: 5000,
        autoplay: true,
        loop: true,
        lazyLoad: true,
        margin: 20,
        dots: false,
        responsiveRefreshRate: 200,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            }
        }
    });
    $navSlider.owlCarousel({
        items: 1,
        slideSpeed: 2000,
        center: true,
        autoplayTimeout: 5000,
        autoplay: true,
        loop: true,
        dots: false,
        nav: true,
        mouseDrag: false,
        touchDrag: false,
        animateIn: 'fadeIn',
        animateOut : 'fadeOut',
        responsiveRefreshRate: 200,
        navContainer: '.home-slider__navigation',
        navText: ['<span class="prev-icon"><svg><use class="icon-use" xlink:href="#icon-long-arrow-left"></use></svg></span>',
            '<span class="next-icon"><svg><use class="icon-use" xlink:href="#icon-long-arrow-right"></use></svg></span>']
    });

    $mainSlider.on('change.owl.carousel', function(event) {
        if (event.namespace && event.property.name === 'position') {
            var target = event.relatedTarget.relative(event.property.value, true);
            $navSlider.owlCarousel('to', target, 300, true);
        }
    });

    $navContainer.on('click', '.owl-next', function () {
        $mainSlider.trigger('next.owl.carousel');
        $mainSlider.trigger('stop.owl.autoplay');
    });
    $navContainer.on('click', '.owl-prev', function () {
        $mainSlider.trigger('prev.owl.carousel');
        $mainSlider.trigger('stop.owl.autoplay');
    });
});
