addAction(INIT, function () {
    var $masonry = $('.post-action-card-masonry');
    if (!$masonry.length) {
        return;
    }
    $masonry.masonry({
        itemSelector: '.post-action-card-masonry__item',
        percentPosition: true
    });
});
