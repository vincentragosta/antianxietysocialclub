addAction(INIT, function() {
    var BREAKPOINT = 300;
    var NARROW_CLASS = 'responsive-component--narrow';
    var LOADED_CLASS = 'responsive-component--loaded';
    var $components = $('.responsive-component');

    if (!$components.length) {
        return;
    }

    addAction(LAYOUT, function() {
        $components.each(function (key, component) {
            var $component = $(component);
            if ($component.width() === 0) {
                return;
            }
            $component.toggleClass(NARROW_CLASS, $component.width() <= BREAKPOINT);
            $component.addClass(LOADED_CLASS);
        });
    });
});
