<?php

use ChildTheme\Components\CommentCard\CommentCardView;
use ChildTheme\Topic\Topic;

global $post;

$Topic = Topic::createFromGlobal();
?>

<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row content-row--mb-half">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <p class="heading"><a href="<?= get_term_link($Topic->category); ?>"><?= $Topic->category->name; ?></a></p>
                    <h1 class="heading heading--large"><?= $Topic->title(); ?></h1>
                    <p class="heading">By <?= $Topic->author()->display_name; ?></p>
                    <ul class="list--inline">
                        <?php foreach ($Topic->getSocialSharer() as $SocialSharer): ?>
                            <li><?= $SocialSharer; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12">
                <div class="content-column__inner">
                    <?= $Topic->content(false); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12">
                <div class="content-column__inner">
                    <?php comment_form(); ?>
                </div>
            </div>
        </div>
        <?php if (!empty($comments = get_comments(['post_id' => $post->ID, 'status' => 'approve']))): ?>
            <?php foreach($comments as $Comment): ?>
                <div class="content-row row">
                    <div class="content-column col-md-12">
                        <div class="content-column__inner">
                            <?= new CommentCardView($Comment); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
