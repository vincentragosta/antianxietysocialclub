<?php

use Backstage\SetDesign\NavMenu\NavMenuView;
use Backstage\SetDesign\SocialIcons\SocialIconsView;
use ChildTheme\Options\GlobalOptions;
?>

<div class="footer-nav">
    <div class="footer-nav__container container">
        <div class="footer-nav__info">
            <?php if (has_nav_menu('footer_navigation')): ?>
                <?= NavMenuView::createList('footer_navigation'); ?>
            <?php endif; ?>
            <p class="footer-nav__copyright heading heading--thin">
                &copy; <?= Date('Y'); ?> AntiAnxietySocialClub, LLC All Rights Reserved
            </p>
        </div>
        <div class="footer-nav__additional-info">
            <div class="footer-nav__social">
                <p class="heading heading--thin">Keep In Touch</p>
                <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                    <?= SocialIconsView::create($social_icons); ?>
                <?php endif; ?>
            </div>
            <div class="footer-nav__attribution">
                <?php if ($compliance_badge = GlobalOptions::complianceBadge()): ?>
                    <?= $compliance_badge
                        ->css_class('footer-nav__compliance')
                        ->width(GlobalOptions::COMPLIANCE_BADGE_SIZE)
                        ->height(GlobalOptions::COMPLIANCE_BADGE_SIZE) ?>
                <?php endif; ?>
                <p class="heading heading--thin">Made By&nbsp;<a href="https://vincentragosta.io" target="_blank">Vincent Ragosta</a></p>
            </div>
        </div>
    </div>
</div>
