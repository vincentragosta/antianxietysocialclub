<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\Message\MessageView;
use Backstage\SetDesign\NavMenu\NavMenuView;
use Backstage\SetDesign\SocialIcons\SocialIconsView;
use ChildTheme\Options\GlobalOptions;
?>

<?php if (GlobalOptions::enableMessaging()): ?>
    <?= MessageView::createGlobal(); ?>
<?php endif; ?>
<header class="header-nav sticky-header" data-gtm="Header">
    <div class="header-nav__brand">
        <a class="header-nav__brand-link" href="<?= home_url() ?>">
            <?php if ($header_image = GlobalOptions::headerBrandImage()): ?>
                <?= $header_image
                    ->css_class('header-nav__brand-image')
                    ->width(GlobalOptions::LOGO_SIZE)
                    ->height(GlobalOptions::LOGO_SIZE) ?>
            <strong class="heading heading--large">Anti Anxiety Social Club</strong>
            <?php else: ?>
                <strong><?php bloginfo('name'); ?></strong>
            <?php endif; ?>
        </a>
    </div>
    <div class="header-nav__container">
        <?php if (has_nav_menu('primary_navigation')): ?>
            <div class="header-nav__menu">
                <?= NavMenuView::createResponsive(); ?>
            </div>
        <?php endif; ?>
        <div class="header-nav__second-menu">
            <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                <?= SocialIconsView::create($social_icons, [], ['class' => 'd-none d-lg-block']); ?>
            <?php endif; ?>
            <hr class="header-nav__line d-none d-lg-block" />
            <div class="header-nav__search">
                <button class="header-nav__search-button button button--inverted">
                    <?= new IconView(['icon_name' => 'search']); ?>
                </button>
            </div>
        </div>
    </div>
    <div class="header-nav__search-drawer">
        <?= get_search_form(); ?>
    </div>
</header>
