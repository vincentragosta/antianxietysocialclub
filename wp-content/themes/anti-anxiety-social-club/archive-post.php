<?php

use Backstage\Models\Page;
use Backstage\SetDesign\Modal\ModalView;
use ChildTheme\Support\ValueObjects\SocialClubTableBanner;
use ChildTheme\Topic\Topic;
use ChildTheme\Components\SocialClubTableRow\SocialClubRowView;
use ChildTheme\Options\GlobalOptions;

if (!($Page = GlobalOptions::socialClubPage()) instanceof Page) {
    return '';
}

global $wp_query, $post;
?>

<div class="content-section">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <?php /* @var Page $Page */ ?>
                    <h1 class="heading heading--large"><?= $Page->title(); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section">
    <div class="container content-section__container">
            <div class="content-row content-row--mb-half row">
                <div class="content-column col-md-12">
                    <div class="content-column__inner">
                        <div class="social-club__navigation">
                            <?= new SocialClubTableBanner($wp_query->post_count); ?>
                            <a href="#submit-topic" class="button">Submit Topic For Review</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php if ($wp_query->have_posts()): ?>
            <div class="content-row row">
                <div class="content-column col-md-12">
                    <div class="content-column__inner">
                        <table class="social-club__topics">
                            <tbody>
                                <tr class="social-club__row social-club__row--headers">
                                    <th class="social-club__data"><strong class="heading heading--medium">Topic</strong></th>
                                    <th class="social-club__data"><strong class="heading heading--medium">Voices</strong></th>
                                    <th class="social-club__data"><strong class="heading heading--medium">Posts</strong></th>
                                    <th class="social-club__data"><strong class="heading heading--medium">Last Post</strong></th>
                                </tr>
                                <?php while($wp_query->have_posts()): the_post(); ?>
                                    <?= new SocialClubRowView(new Topic($post)); ?>
                                <?php endwhile; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php if ($wp_query->max_num_pages > 1): ?>
                <div class="content-row row">
                    <div class="content-column text--center col-md-12">
                        <div class="content-column__inner">
                            <?php the_posts_pagination(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php else: ?>
            <div class="content-row row">
                <div class="content-column text--center col-md-12">
                    <div class="content-column__inner">
                        <strong class="heading">No topics currently exist.</strong>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?= ModalView::create('submit-topic', 'box', do_shortcode('[gravityform id="2"]')); ?>
