<?php

use Backstage\Producers\RelatedContent\RelatedContentService;
use ChildTheme\Article\RelatedArticleRepository;
use ChildTheme\Components\PostActionCard\PostActionCardView;
use ChildTheme\Recipe\Recipe;
use ChildTheme\Recipe\RecipeRepository;

$Recipe = Recipe::createFromGlobal();
$RelatedArticleService = new RelatedContentService($Recipe, new RelatedArticleRepository(), 4);
$RelatedArticleCollection = $RelatedArticleService->getRelatedContent();

$RelatedRecipeService = new RelatedContentService($Recipe, new RecipeRepository(), 4);
$RelatedRecipeCollection = $RelatedRecipeService->getRelatedContent();
?>

<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row content-row--mb-half">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <p class="heading">
                        <a href="<?= get_post_type_archive_link(Recipe::POST_TYPE); ?>">Recipes</a>
                    </p>
                    <h1 class="heading heading--large"><?= $Recipe->title(); ?></h1>
                    <p class="heading">By <?= $Recipe->author()->display_name; ?></p>
                    <ul class="list--inline">
                        <?php foreach($Recipe->getSocialSharer() as $SocialSharer): ?>
                            <li><?= $SocialSharer; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <?= $Recipe->featuredImage(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12">
                <div class="content-column__inner">
                    <?= $Recipe->content(false); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12">
                <div class="content-column__inner">
                    <div class="recipe-details">
                        <div class="recipe-details__information">
                            <?php if (($RecipeType = $Recipe->recipe_type) instanceof \WP_Term): ?>
                                <div class="recipe-details__item recipe-details__item--type">
                                    <h3 class="heading">Recipe Type:</h3>
                                    <p><?= $RecipeType->name; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($dietary_info = $Recipe->dietary_info): ?>
                                <div class="recipe-details__item recipe-details__item--dietary-info">
                                    <h3 class="heading">Dietary Info:</h3>
                                    <p><?= $dietary_info; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($serving_size = $Recipe->serving_size): ?>
                                <div class="recipe-details__item recipe-details__item--serving-size">
                                    <h3 class="heading">Serving Size:</h3>
                                    <p><?= $serving_size; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($prep_time = $Recipe->prep_time): ?>
                                <div class="recipe-details__item recipe-details__item--prep-time">
                                    <h3 class="heading">Prep Time:</h3>
                                    <p><?= $prep_time; ?> min.</p>
                                </div>
                            <?php endif; ?>
                            <?php if ($cook_time = $Recipe->cook_time): ?>
                                <div class="recipe-details__item recipe-details__item--cook-time">
                                    <h3 class="heading">Cook Time:</h3>
                                    <p><?= $cook_time; ?> min.</p>
                                </div>
                            <?php endif; ?>
                            <?php if (($total_time = $Recipe->getTotalTime()) > 0): ?>
                                <div class="recipe-details__item recipe-details__item--total-time">
                                    <h3 class="heading">Total Time:</h3>
                                    <p><?= $total_time; ?> min.</p>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="recipe-details__ingredients-instructions">
                        <?php if (!empty($ingredients = $Recipe->ingredients)): ?>
                            <div class="recipe-details__ingredients">
                                <h3 class="heading">Ingredients</h3>
                                <ul class="list--unstyled">
                                    <?php foreach($ingredients as $ingredient): ?>
                                        <li><?= $ingredient['item']; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($instructions = $Recipe->instructions)): ?>
                            <div class="recipe-details__instructions">
                                <h3 class="heading">Instructions</h3>
                                <ol>
                                    <?php foreach($instructions as $instruction): ?>
                                        <li><?= $instruction['item']; ?></li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!$RelatedArticleCollection->isEmpty()): ?>
    <div class="content-section content-section--width-narrow content-section--has-bg content-section--gray content-section--mb-none">
        <div class="container content-section__container">
            <div class="content-row row">
                <div class="content-column text--center col-md-12">
                    <div class="content-column__inner">
                        <h2 class="heading heading--large">Related Articles</h2>
                    </div>
                </div>
            </div>
            <div class="content-row row">
                <?php foreach($RelatedArticleCollection as $Article) : ?>
                    <div class="content-column col-md-3">
                        <div class="content-column__inner">
                            <?= new PostActionCardView($Article); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (!$RelatedRecipeCollection->isEmpty()): ?>
    <div class="content-section content-section--width-narrow content-section--mb-none">
        <div class="container content-section__container">
            <div class="content-row row">
                <div class="content-column text--center col-md-12">
                    <div class="content-column__inner">
                        <h2 class="heading heading--large">Related recipes</h2>
                    </div>
                </div>
            </div>
            <div class="content-row row">
                <?php foreach($RelatedRecipeCollection as $Recipe): ?>
                    <div class="content-column col-md-3">
                        <div class="content-column__inner">
                            <?= new PostActionCardView($Recipe); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
