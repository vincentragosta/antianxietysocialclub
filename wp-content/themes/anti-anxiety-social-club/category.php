<?php

use Backstage\Models\PostFactory;
use ChildTheme\Components\PostActionCard\PostActionCardView;

global $wp_query, $post;

/* @var WP_Term $Term */
$Term = get_queried_object();

if (!$Term instanceof WP_Term) {
    return 'Category Not Set.';
}
?>

<div class="content-section">
    <div class="container content-section__container">
        <div class="content-row row content-row--mb-half">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <h1 class="heading heading--large">Category: <?= $Term->name; ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($wp_query->have_posts()): ?>
    <div class="content-section">
        <div class="container content-section__container">
            <?php while($wp_query->have_posts()): the_post(); ?>
                <div class="content-row row content-row--mb-half">
                    <div class="content-column col-md-12">
                        <div class="content-column__inner">
                            <?= new PostActionCardView(PostFactory::create($post)); ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php if ($wp_query->max_num_pages > 1): ?>
        <div class="content-section">
            <div class="container content-section__container">
                <div class="content-row row">
                    <div class="content-column text--center col-md-12">
                        <div class="content-column__inner">
                            <?php the_posts_pagination(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php else: ?>
    <div class="content-section">
        <div class="container content-section__container">
            <div class="content-row row">
                <div class="content-column text--center col-md-12">
                    <div class="content-column__inner">
                        <strong class="heading">No articles currently exist.</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
