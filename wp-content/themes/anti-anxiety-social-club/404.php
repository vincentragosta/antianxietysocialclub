<div class="content-section content-section--width-narrow content-section--height-full">
    <div class="container content-section__container">
        <div class="content-row row content-row--mb-half">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <h1 class="heading heading--xlarge">Sorry, but the page you were trying to view does not exist.</h1>
                </div>
            </div>
        </div>
    </div>
</div>
