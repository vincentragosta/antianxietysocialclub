<?php

use Backstage\Producers\RelatedContent\RelatedContentService;
use ChildTheme\Article\Article;
use ChildTheme\Article\ArticleRepository;
use ChildTheme\Components\CommentCard\CommentCardView;
use ChildTheme\Components\PostActionCard\PostActionCardView;
use ChildTheme\Recipe\RelatedRecipeRepository;

global $post;

$Article = Article::createFromGlobal();
$RelatedArticleService = new RelatedContentService($Article, new ArticleRepository(), 4);
$RelatedArticleCollection = $RelatedArticleService->getRelatedContent();

$RelatedRecipeService = new RelatedContentService($Article, new RelatedRecipeRepository(), 4);
$RelatedRecipeCollection = $RelatedRecipeService->getRelatedContent();
?>

<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row content-row--mb-half">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <p class="heading"><a href="<?= get_term_link($Article->category); ?>"><?= $Article->category->name; ?></a></p>
                    <h1 class="heading heading--large"><?= $Article->title(); ?></h1>
                    <p class="heading">By <?= $Article->author()->display_name; ?></p>
                    <ul class="list--inline">
                        <?php foreach ($Article->getSocialSharer() as $SocialSharer): ?>
                            <li><?= $SocialSharer; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column text--center col-md-12">
                <div class="content-column__inner">
                    <?= $Article->featuredImage(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12">
                <div class="content-column__inner">
                    <?= $Article->content(false); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!$RelatedArticleCollection->isEmpty()): ?>
    <div class="content-section content-section--width-narrow">
        <div class="container content-section__container">
            <div class="content-row row">
                <div class="content-column text--center col-md-12">
                    <div class="content-column__inner">
                        <h2 class="heading heading--large">Related Articles</h2>
                    </div>
                </div>
            </div>
            <div class="content-row row">
                <?php foreach ($RelatedArticleCollection as $Article) : ?>
                    <div class="content-column col-md-3">
                        <div class="content-column__inner">
                            <?= new PostActionCardView($Article); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (!$RelatedRecipeCollection->isEmpty()): ?>
    <div class="content-section content-section--width-narrow">
        <div class="container content-section__container">
            <div class="content-row row">
                <div class="content-column text--center col-md-12">
                    <div class="content-column__inner">
                        <h2 class="heading heading--large">Related Recipes</h2>
                    </div>
                </div>
            </div>
            <div class="content-row row">
                <?php foreach ($RelatedRecipeCollection as $Recipe) : ?>
                    <div class="content-column col-md-3">
                        <div class="content-column__inner">
                            <?= new PostActionCardView($Recipe); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="content-section content-section--width-narrow">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12">
                <div class="content-column__inner">
                    <?php comment_form(); ?>
                </div>
            </div>
        </div>
        <?php if (!empty($comments = get_comments(['post_id' => $post->ID, 'status' => 'approve']))): ?>
            <?php foreach($comments as $Comment): ?>
                <div class="content-row row">
                    <div class="content-column col-md-12">
                        <div class="content-column__inner">
                            <?= new CommentCardView($Comment); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
