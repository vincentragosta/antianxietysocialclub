<?php

namespace ChildTheme\Article;

use Backstage\Producers\RelatedContent\RelatedContentRepository;

/**
 * Class ArticleRepository
 * @package ChildTheme\Article
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class ArticleRepository extends RelatedContentRepository
{
    protected $model_class = Article::class;
    protected $related_model_class = Article::class;
}
