<?php

namespace ChildTheme\Article;

use Backstage\Producers\RelatedContent\RelatedContentRepository;
use ChildTheme\Recipe\Recipe;

/**
 * Class RelatedArticleRepository
 * @package ChildTheme\Article
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class RelatedArticleRepository extends RelatedContentRepository
{
    protected $model_class = Article::class;
    protected $related_model_class = Recipe::class;
}
