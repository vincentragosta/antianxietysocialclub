<?php

namespace ChildTheme\Article;

class ArticleController
{
    public function __construct()
    {
        add_filter('pre_get_posts', [$this, 'addArticlesToCategoryQuery']);
    }

    public function addArticlesToCategoryQuery(\WP_Query $wp_query)
    {
        if (!$wp_query->is_admin() && $wp_query->is_category() && $wp_query->is_main_query()) {
            $wp_query->set('post_type', Article::POST_TYPE);
        }
        return $wp_query;
    }
}
