<?php

namespace ChildTheme\Article;

use ChildTheme\Support\Interfaces\HasCategoryLink;
use ChildTheme\Support\Models\RelatedContentPost;
use \WP_Term;

/**
 * Class Article
 * @package ChildTheme\Article
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $category
 * @property array $related_recipe_id
 */
class Article extends RelatedContentPost implements HasCategoryLink
{
    const TAXONOMY = 'category';
    const RELATED_TAXONOMY = 'category';
    const POST_TYPE = 'article';

    protected function getCategory()
    {
        if (empty($category = $this->field(static::TAXONOMY))) {
            return '';
        }
        if (is_array($category)) {
            $category = $category[0];
        }
        return new WP_Term($category);
    }

    public function getCategoryLink(): array
    {
        if (!($category = $this->getCategory()) instanceof WP_Term) {
            return [];
        }
        return [
            'title' => $category->name,
            'url' => get_term_link($category)
        ];
    }
}
