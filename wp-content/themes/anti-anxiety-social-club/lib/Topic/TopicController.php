<?php

namespace ChildTheme\Topic;

/**
 * Class TopicController
 * @package ChildTheme\Topic
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class TopicController
{
    public function __construct()
    {
        add_filter('register_post_type_args', [$this, 'changeRewriteRules'], 10, 2);
    }

    public function changeRewriteRules($args, $post_type)
    {
        if ($post_type == Topic::POST_TYPE) {
            $args['has_archive'] = 'social-club';
            $args['rewrite'] = [
                'with_front' => false,
                'slug' => 'topics'
            ];
        }
        return $args;
    }
}
