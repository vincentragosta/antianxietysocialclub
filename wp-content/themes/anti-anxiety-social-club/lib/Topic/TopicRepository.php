<?php

namespace ChildTheme\Topic;

use Backstage\Repositories\PostRepository;

/**
 * Class TopicRepository
 * @package ChildTheme\Topic
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class TopicRepository extends PostRepository
{
    protected $model_class = Topic::class;
}
