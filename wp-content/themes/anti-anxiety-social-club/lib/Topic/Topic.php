<?php

namespace ChildTheme\Topic;

use Backstage\Support\DateTime;
use ChildTheme\SocialSharer\SocialSharerPost;
use ChildTheme\Support\Interfaces\HasCategoryLink;
use ChildTheme\Support\RelativeTimeHandler;

/**
 * Class Topic
 * @package ChildTheme\Topic
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property \WP_Term|string $category
 * @property array $authors
 */
class Topic extends SocialSharerPost implements HasCategoryLink
{
    const LABEL = 'Topic';
    const POST_TYPE = 'post';
    const TAXONOMY = 'category';

    protected $authors = [];

    public function getComments()
    {
        return get_comments(['post_id' => $this->ID]);
    }

    public function getTotalComments()
    {
        return count($this->getComments());
    }

    public function getTotalCommentAuthors()
    {
        return count($this->getCommentAuthors());
    }

    protected function getCommentAuthors()
    {
        if (empty($this->authors)) {
            $comment_authors = array_map(function (\WP_Comment $Comment) {
                return $Comment->comment_author;
            }, $this->getComments());
            foreach ($comment_authors as $author_name) {
                if (!in_array($author_name, $this->authors)) {
                    $this->authors[] = $author_name;
                }
            }
        }
        return $this->authors;
    }

    protected function getLastComment()
    {
        return !empty($comments = $this->getComments()) ? $comments[0] : false;
    }

    public function getLastCommentRelativeDate()
    {
        if (!$LastComment = $this->getLastComment()) {
            return '';
        }
        $last_comment_date = new DateTime($LastComment->comment_date);
        return (new RelativeTimeHandler($last_comment_date->getTimestamp()))->getRelativeTime();
    }

    protected function getCategory()
    {
        if (empty($category = $this->terms(static::TAXONOMY))) {
            return '';
        }
        if (is_array($category)) {
            $category = $category[0];
        }
        return new \WP_Term($category);
    }

    public function getCategoryLink(): array
    {
        if (!($category = $this->getCategory()) instanceof \WP_Term) {
            return [];
        }
        return [
            'title' => $category->name,
            'url' => get_term_link($category)
        ];
    }
}
