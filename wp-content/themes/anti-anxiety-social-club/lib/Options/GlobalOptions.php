<?php

namespace ChildTheme\Options;

use Backstage\Models\Page;
use Orchestrator\GlobalOptions as GlobalOptionsBase;

use WP_Image;

/**
 * Class GlobalOptions
 * @package ChildTheme\Options
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @method static WP_Image headerBrandImage()
 * @method static complianceBadge()
 * @method static socialIcons()
 * @method static recipeCategory()
 * @method static socialClubPage()
 */
class GlobalOptions extends GlobalOptionsBase
{
    const COMPLIANCE_BADGE_SIZE = 25;
    const LOGO_SIZE = 50;

    protected $default_values = [
        'header__brand_image' => null,
        'compliance_badge' => '',
        'social_icons' => [],
        'recipe_category' => '',
        'social_club_page' => null
    ];

    public function getComplianceBadge()
    {
        return \WP_Image::get_by_attachment_id($this->get('compliance_badge'));
    }

    public function getRecipeCategory()
    {
        if (empty($category = $this->get('recipe_category'))) {
            return false;
        }
        return get_term($category);
    }

    public function getSocialClubPage()
    {
        if (empty($page_id = $this->get('social_club_page'))) {
            return false;
        }
        return new Page($page_id);
    }
}
