<?php

namespace ChildTheme\Components\PostActionCard;

use Backstage\Models\PostBase;
use Backstage\Models\PostFactory;
use Backstage\VcLibrary\Support\Component;
use ChildTheme\Article\Article;
use ChildTheme\Recipe\Recipe;

/**
 * Class PostActionCard
 * @package ChildTheme\Components\PostActionCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class PostActionCard extends Component
{
    const NAME = 'Post Action Card';
    const TAG = 'post_action_card';
    const VIEW = PostActionCardView::class;

    protected static $post_type_classes = [
        Article::class, Recipe::class
    ];

    protected $component_config = [
        'description' => 'Create a card from an article or recipe.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'posts' => [
                'type' => 'dropdown',
                'heading' => 'Post',
                'param_name' => 'post_id',
                'value' => '',
                'description' => 'Select an article or recipe.',
                'admin_label' => true,
            ],
        ]
    ];

    protected function setupConfig()
    {
        parent::setupConfig();
        $this->setPosts();
    }

    protected function setPosts()
    {
        $options['-- Select Post --'] = '';
        foreach (static::$post_type_classes as $class) {
            if (!is_a($class, PostBase::class, true)) {
                continue;
            }
            $post_type_object = get_post_type_object($class::POST_TYPE);
            if (!$post_type_object) {
                continue;
            }
            $name = $post_type_object->labels->name;
            $options["-------- $name --------"] = 0;
            foreach ($class::getPosts(['posts_per_page' => -1]) as $Post) { /* @var PostBase $Post */
                $options[$Post->post()->post_title] = $Post->ID;
            }
        }
        $this->component_config['params']['posts']['value'] = $options;
    }

    protected function createView(array $atts)
    {
        /* @var PostActionCardView $ViewClass */
        $ViewClass = static::VIEW;
        if (empty($post_id = $atts['post_id'])) {
            return '';
        }
        return new $ViewClass((new PostFactory())->create($post_id));
    }
}
