<?php
/**
 * Expected:
 * @var string $url
 * @var string $image
 * @var string $read_more
 * @var string $term
 * @var string $title
 * @var string $content
 * @var array $social_sharer
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use Backstage\View\Link;

$element_attributes['class'] = 'responsive-component';
?>

<div <?= Util::componentAttributes('post-action-card', $class_modifiers, $element_attributes); ?>>
    <?php if ($image): ?>
        <div class="post-action-card__image-container">
            <?= $image; ?>
            <a href="<?= $url; ?>" class="post-action-card__image-overlay">
                <strong class="heading">Read<br />More</strong>
            </a>
        </div>
    <?php endif; ?>
    <div class="post-action-card__content-container">
        <?php if (!empty($term)): ?>
            <div class="post-action-card__category heading heading--thin"><?= Link::createFromField($term); ?></div>
        <?php endif; ?>
        <?php if (!empty($title)): ?>
            <h3 class="post-action-card__title heading heading--large"><?= new Link($url, $title); ?></h3>
        <?php endif; ?>
        <?php if (!empty($social_sharer)): ?>
            <div class="post-action-card__sharer">
                <ul class="list--inline">
                    <li class="heading heading--thin">Share</li>
                    <?php foreach ($social_sharer as $SocialSharer): ?>
                        <li><?= $SocialSharer; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
        <?php if ($content): ?>
            <div class="post-action-card__content">
                <?= $content; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
