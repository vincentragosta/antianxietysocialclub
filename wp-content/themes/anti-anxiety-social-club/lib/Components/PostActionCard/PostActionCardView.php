<?php

namespace ChildTheme\Components\PostActionCard;

use Backstage\Models\PostBase;
use Backstage\View\Component;

/**
 * Class PostActionCardView
 * @package ChildTheme\Components\PostActionCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $term
 * @property string $title
 * @property string $content
 * @property string $image
 * @property string $url
 * @property array $social_sharer
 * @property array $class_modifiers
 * @property array $element_attributes
 */
class PostActionCardView extends Component
{
    const TRIM_WORD_COUNT = 75;
    const IMAGE_WIDTH = 768;
    const IMAGE_HEIGHT = 914;

    protected $name = 'post-action-card';
    protected static $default_properties = [
        'term' => '',
        'title' => '',
        'content' => '',
        'image' => false,
        'url' => '',
        'social_sharer' => []
    ];

    public function __construct(PostBase $Post)
    {
        parent::__construct([
            'url' => $Post->permalink(),
            'term' => $Post->getCategoryLink(),
            'title' => $Post->title(),
            'content' => $Post->content(false),
            'image' => apply_filters('post-action-card/curate-image', '__return_true') ?
                $this->curateImage($Post->featuredImage()) : $Post->featuredImage(),
            'social_sharer' => $Post->getSocialSharer()
        ]);
        if ($this->content) {
            $this->content = wp_trim_words($this->content, static::TRIM_WORD_COUNT, '&hellip;');
        }
        if (!$this->image instanceof \WP_Image) {
            $this->classModifiers('no-image');
        }
    }

    protected function curateImage($image)
    {
        if (!$image instanceof \WP_Image) {
            return $image;
        }
        return $image->width(static::IMAGE_WIDTH)->height(static::IMAGE_HEIGHT);
    }
}
