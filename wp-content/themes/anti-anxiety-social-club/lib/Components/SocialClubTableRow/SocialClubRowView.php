<?php

namespace ChildTheme\Components\SocialClubTableRow;

use Backstage\View\Component;
use ChildTheme\Topic\Topic;

/**
 * Class SocialClubRowView
 * @package ChildTheme\Components\SocialClubTableRow
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $topic
 * @property string $permalink
 * @property string $voices
 * @property string $author_name
 * @property string $total_posts
 * @property string $last_post
 */
class SocialClubRowView extends Component
{
    protected $name = 'social-club-row';
    protected static $default_properties = [
        'topic' => '',
        'permalink' => '',
        'voices' => '',
        'author_name' => '',
        'total_posts' => '',
        'last_post' => ''
    ];

    public function __construct(Topic $Topic)
    {
        parent::__construct([
            'topic' => $Topic->title(),
            'permalink' => $Topic->permalink(),
            'voices' => $Topic->getTotalCommentAuthors(),
            'author_name' => $Topic->author()->display_name,
            'total_posts' => $Topic->getTotalComments(),
            'last_post' => $Topic->getLastCommentRelativeDate()
        ]);
    }
}
