<?php
/**
 * Expected:
 * @var string $topic
 * @var string $permalink
 * @var string $voices
 * @var string $author_name
 * @var string $total_posts
 * @var string $last_post
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (empty($topic) || empty($permalink)) {
    return '';
}
?>

<tr <?= Util::componentAttributes('social-club__row', $class_modifiers, $element_attributes); ?>>
    <td class="social-club__data">
        <a href="<?= $permalink; ?>" class="social-club__topic">
            <strong class="heading heading--medium heading--thin"><?= $topic; ?></strong>
        </a>
        <?php if ($author_name): ?>
            <p class="social-club__author heading heading--thin">Started by: <?= $author_name; ?></p>
        <?php endif; ?>
    </td>
    <td class="social-club__data social-club__data--voices"><strong class="heading heading--thin"><?= $voices; ?></strong></td>
    <td class="social-club__data social-club__data--total-posts"><strong class="heading heading--thin"><?= $total_posts; ?></strong></td>
    <td class="social-club__data social-club__data--last-post">
        <?php if ($last_post): ?>
            <a href="<?= $permalink; ?>" class="social-club__last-post"><strong class="heading heading--thin"><?= $last_post; ?></strong></a>
            <?php if ($author_name): ?>
                <p class="heading heading--thin"><?= $author_name; ?></p>
            <?php endif; ?>
        <?php endif; ?>
    </td>
</tr>
