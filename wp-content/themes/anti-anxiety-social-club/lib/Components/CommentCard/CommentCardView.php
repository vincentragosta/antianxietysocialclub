<?php

namespace ChildTheme\Components\CommentCard;

use Backstage\Support\DateTime;
use Backstage\View\Component;

/**
 * Class CommentCardView
 * @package ChildTheme\Components\CommentCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $author_name
 * @property string $author_email
 * @property string $date
 * @property string $content
 */
class CommentCardView extends Component
{
    const DATE_FORMAT = 'F j, Y';

    protected $name = 'comment-card';
    protected static $default_properties = [
        'author_name' => '',
        'author_email' => '',
        'date' => '',
        'content' => ''
    ];

    public function __construct(\WP_Comment $Comment)
    {
        parent::__construct([
            'author_name' => $Comment->comment_author,
            'author_email' => $Comment->comment_author_email,
            'date' => $this->getFormattedDate($Comment->comment_date),
            'content' => $Comment->comment_content
        ]);
    }

    protected function getFormattedDate($date)
    {
        if (empty($date)) {
            return '';
        }
        try {
            return new DateTime($date, static::DATE_FORMAT);
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }
}
