<?php
/**
 * Expected:
 * @var string $author_name
 * @var string $author_email
 * @var string $date
 * @var string $content
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (empty($content) || empty($author_name) || empty($date)) {
    return '';
}
?>

<div <?= Util::componentAttributes('comment-card', $class_modifiers, $element_attributes); ?>>
    <h2 class="comment-card__author heading heading--large"><?= $author_name; ?></h2>
    <?php if (!empty($author_email)): ?>
        <strong class="heading"><?= $author_email; ?></strong>
    <?php endif; ?>
    <p class="comment-card__date heading">Posted: <?= $date; ?></p>
    <p class="comment-card__content"><?= $content; ?></p>
</div>
