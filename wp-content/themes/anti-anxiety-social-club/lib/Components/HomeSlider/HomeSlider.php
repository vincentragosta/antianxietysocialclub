<?php

namespace ChildTheme\Components\HomeSlider;

use Backstage\VcLibrary\Support\ParentComponent;
use Backstage\View\ViewCollection;

/**
 * Class PostActionCardSlider
 * @package ChildTheme\Components\HomeSlider
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $content
 */
class HomeSlider extends ParentComponent
{
    const NAME = 'Home Slider';
    const TAG = 'home_slider';
    const VIEW = HomeSliderView::class;
    protected $children = ['home_slide'];

    protected $component_config = [
        'description' => 'Create content slider.',
        'is_container' => true,
        'content_element' => true,
        'show_settings_on_create' => false,
        'js_view' => 'VcColumnView',
        'category' => 'Structure'
    ];

    protected function loadTemplate($atts, $content = null)
    {
        /* @var ViewCollection $content */
        return $this->createView([
            'content' => $content->getAll()
        ]);
    }

    /**
     * @param array $atts
     * @return mixed
     */
    protected function createView(array $atts)
    {
        /* @var HomeSliderView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass(new ViewCollection($atts['content']));
    }
}
