<?php

namespace ChildTheme\Components\HomeSlider;

use Backstage\View\Component;

/**
 * Class HomeSliderNavItemView
 * @package ChildTheme\Components\HomeSlider
 * @property string $headline
 * @property string $url
 */
class HomeSliderNavItemView extends Component
{
    protected $name = 'home-slider-nav-item';

    public function __construct(HomeSliderCardView $View)
    {
        parent::__construct([
            'title' => $View->PostBase->title(),
            'category' => $View->PostBase->getCategoryLink()
        ]);
    }
}
