<?php
/**
 * Expected:
 * @var PostBase $PostBase
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Models\PostBase;
use Backstage\Util;
use ChildTheme\Components\PostActionCard\PostActionCardView;

if (!($image = $PostBase->featuredImage()) instanceof \WP_Image || empty($permalink = $PostBase->permalink())) {
    return '';
}
?>

<div <?= Util::componentAttributes('home-slider-card', $class_modifiers, $element_attributes); ?>>
    <a href="<?= $permalink; ?>">
        <?= $image
            ->css_class('home-slider-card__image')
            ->height(PostActionCardView::IMAGE_HEIGHT)
            ->width(PostActionCardView::IMAGE_WIDTH); ?>
    </a>
</div>
