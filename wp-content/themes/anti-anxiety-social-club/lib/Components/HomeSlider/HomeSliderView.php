<?php

namespace ChildTheme\Components\HomeSlider;

use Backstage\View\Component;
use Backstage\View\ViewCollection;

/**
 * Class HomeSliderView
 * @package ChildTheme\Components\HomeSlider
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class HomeSliderView extends Component
{
    protected $name = 'home-slider';
    protected static $default_properties = [
        'content' => ''
    ];

    public function __construct(ViewCollection $ViewCollection)
    {
        parent::__construct([
            'slides' => $ViewCollection,
            'nav_items' => new ViewCollection($ViewCollection->map(function($View) {
                return new HomeSliderNavItemView($View);
            }))
        ]);
    }
}
