<?php
/**
 * Expected:
 * @var ViewCollection $slides
 * @var ViewCollection $nav_items
 */

use Backstage\View\ViewCollection;
use Backstage\Util;

?>

<section <?= Util::componentAttributes('home-slider'); ?>>
    <h2 class="sr-only">Article and/or Recipe Slider</h2>
    <div class="home-slider__main owl-carousel">
        <?= $slides; ?>
    </div>
    <div class="home-slider__nav-container">
        <div class="home-slider__navigation"></div>
        <div class="home-slider__nav owl-carousel">
            <?= $nav_items ?>
        </div>
    </div>
</section>
