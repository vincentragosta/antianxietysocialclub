<?php

namespace ChildTheme\Components\HomeSlider;

use Backstage\Models\PostBase;
use Backstage\View\Component;

/**
 * Class HomeSliderCardView
 * @package ChildTheme\Components\HomeSlider
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property \WP_Image|bool $image
 * @property PostBase $PostBase
 */
class HomeSliderCardView extends Component
{
    protected $name = 'home-slider-card';
    protected static $default_properties = [
        'PostBase' => null
    ];

    public function __construct(PostBase $PostBase)
    {
        parent::__construct(compact('PostBase'));
    }
}
