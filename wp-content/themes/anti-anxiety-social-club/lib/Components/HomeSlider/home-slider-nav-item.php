<?php
/**
 * Expected:
 * @var string $category
 * @var string $title
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use Backstage\View\Link;

if (empty($title)) {
    return '';
}

$element_attributes['class'] = 'text--center';
?>

<div <?= Util::componentAttributes('home-slider-nav-item', $class_modifiers, $element_attributes); ?>>
    <?php if (!empty($category)): ?>
        <strong class="home-slider-nav-item__category heading heading--thin"><?= Link::createFromField($category); ?></strong>
    <?php endif; ?>
    <h2 class="home-slider-nav-item__title heading heading--large"><?= $title; ?></h2>
</div>
