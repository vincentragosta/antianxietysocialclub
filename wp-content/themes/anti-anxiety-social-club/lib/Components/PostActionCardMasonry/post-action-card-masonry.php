<?php
/**
 * Expected:
 * @var PostActionCardView[] $posts
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use ChildTheme\Components\PostActionCard\PostActionCardView;

if (empty($posts)) {
    return '';
}

$element_attributes['class'] = 'content-row_inner row';
?>

<div <?= Util::componentAttributes('post-action-card-masonry', $class_modifiers, $element_attributes); ?>>
    <?php foreach($posts as $PostActionCardView): ?>
        <div class="post-action-card-masonry__item content-column__inner col-md-6">
            <?= $PostActionCardView; ?>
        </div>
    <?php endforeach; ?>
</div>
