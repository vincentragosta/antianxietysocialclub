<?php

namespace ChildTheme\Components\PostActionCardMasonry;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\Article\ArticleRepository;

/**
 * Class PostActionCardMasonry
 * @package ChildTheme\Components\PostActionCardMasonry
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class PostActionCardMasonry extends Component
{
    const NAME = 'Post Action Card Masonry';
    const TAG = 'post_action_card_masonry';
    const VIEW = PostActionCardMasonryView::class;

    protected $component_config = [
        'description' => 'Set a masonry of post action cards.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'categories' => [
                'type' => 'checkbox',
                'heading' => 'Category',
                'param_name' => 'cat_id',
                'value' => [],
                'description' => 'Required. Select the categories to display',
                'admin_label' => true
            ],
            'posts_per_page' => [
                'type' => 'dropdown',
                'heading' => 'Posts Per Page',
                'param_name' => 'posts_per_page',
                'value' => [
                    '-- Select --' => '',
                    '4' => 4,
                    '6' => 6,
                    '8' => 8,
                ],
                'description' => 'Required. Select how many posts to display',
                'admin_label' => true
            ]
        ]
    ];

    protected function setupConfig()
    {
        parent::setupConfig();
        $this->setCategories();
    }

    protected function setCategories()
    {
        $options = [];
        $categories = get_categories(['childless' => true]);
        foreach ($categories as $Category) {
            /* @var \WP_Term $Category */
            $options[$Category->name] = $Category->term_id;
        }
        $this->component_config['params']['categories']['value'] = $options;
    }

    protected function createView(array $atts)
    {
        /* @var PostActionCardMasonryView $ViewClass */
        $ViewClass = static::VIEW;
        if (empty($posts_per_page = $atts['posts_per_page']) || empty($cat_id = $atts['cat_id'])) {
            return '';
        }
        return new $ViewClass((new ArticleRepository())->find([
            'posts_per_page' => $posts_per_page,
            'cat' => $cat_id
        ]));
    }
}
