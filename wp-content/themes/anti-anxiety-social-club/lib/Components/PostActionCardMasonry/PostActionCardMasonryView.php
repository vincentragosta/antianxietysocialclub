<?php

namespace ChildTheme\Components\PostActionCardMasonry;

use Backstage\View\Component;
use ChildTheme\Components\PostActionCard\PostActionCardView;

/**
 * Class PostActionCardMasonryView
 * @package ChildTheme\Components\ArticleCardmasonry
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property PostActionCardView[] $posts
 * @property array $class_modifiers
 * @property array $element_attributes
 */
class PostActionCardMasonryView extends Component
{
    protected $name = 'post-action-card-masonry';
    protected static $default_properties = [
        'posts' => []
    ];

    public function __construct(array $posts)
    {
        add_filter('post-action-card/curate-image', '__return_false');
        $posts = array_map(function($Post) {
            return new PostActionCardView($Post);
        }, $posts);
        add_filter('post-action-card/curate-image', '__return_true');
        parent::__construct(compact('posts'));
    }
}
