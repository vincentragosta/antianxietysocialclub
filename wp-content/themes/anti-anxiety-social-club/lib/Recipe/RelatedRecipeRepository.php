<?php

namespace ChildTheme\Recipe;

use Backstage\Producers\RelatedContent\RelatedContentRepository;
use ChildTheme\Article\Article;

/**
 * Class RelatedRecipeRepository
 * @package ChildTheme\Recipe
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class RelatedRecipeRepository extends RelatedContentRepository
{
    protected $model_class = Recipe::class;
    protected $related_model_class = Article::class;
}
