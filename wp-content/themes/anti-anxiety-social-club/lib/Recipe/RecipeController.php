<?php

namespace ChildTheme\Recipe;

use ChildTheme\Article\Article;
use ChildTheme\Options\GlobalOptions;

/**
 * Class RecipeController
 * @package ChildTheme\Recipe
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class RecipeController
{
    public function __construct()
    {
        add_filter('after_save_post_recipe', [$this, 'setCategoryOnRecipe']);
        add_action('init', function() {
            add_filter('pre_get_posts', [$this, 'addRecipesToCategoryQuery']);
        });
    }

    public function setCategoryOnRecipe($recipe_id)
    {
        if (empty($RecipeCategory = GlobalOptions::recipeCategory())) {
            return;
        }
        wp_set_post_terms($recipe_id, [$RecipeCategory->term_id], Article::RELATED_TAXONOMY);
    }

    public function addRecipesToCategoryQuery(\WP_Query $wp_query) {
        if ($wp_query->is_admin() || !$wp_query->is_main_query()) {
            return $wp_query;
        }
        if (empty($RecipeCategory = GlobalOptions::recipeCategory())) {
            return $wp_query;
        }
        if ($wp_query->is_category($RecipeCategory->term_id) && $wp_query->is_main_query()) {
            $wp_query->set('post_type', [Article::POST_TYPE, Recipe::POST_TYPE]);
        }
        return $wp_query;
    }
}
