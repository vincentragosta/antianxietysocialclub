<?php

namespace ChildTheme\Recipe;

use ChildTheme\Support\Interfaces\HasCategoryLink;
use ChildTheme\Support\Models\RelatedContentPost;

/**
 * Class Recipe
 * @package ChildTheme\Recipe
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $dietary_info
 * @property string $serving_size
 * @property string $prep_time
 * @property string $cook_time
 * @property array $ingredients
 * @property array $instructions
 * @property string $related_post_id
 * @property string $recipe_type
 */
class Recipe extends RelatedContentPost implements HasCategoryLink
{
    const POST_TYPE = 'recipe';
    const RELATED_TAXONOMY = 'recipe-type';
    const POST_ACTION_TERM_LABEL = 'Recipes';

    protected function getRecipeType()
    {
        if (is_array($recipe_type = $this->terms(static::RELATED_TAXONOMY))) {
            $recipe_type = $recipe_type[0];
        }
        return new \WP_Term($recipe_type);
    }

    public function getCategoryLink(): array
    {
        return [
            'title' =>  static::POST_ACTION_TERM_LABEL,
            'url' => get_post_type_archive_link(static::POST_TYPE)
        ];
    }

    public function getTotalTime()
    {
        $prep_time = $this->field('prep_time') ?: 0;
        $cook_time = $this->field('cook_time') ?: 0;
        return $prep_time + $cook_time;
    }
}
