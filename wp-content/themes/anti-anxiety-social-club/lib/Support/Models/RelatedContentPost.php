<?php

namespace ChildTheme\Support\Models;

use Backstage\Producers\RelatedContent\HasRelatedContent;
use Backstage\Producers\RelatedContent\RelatedContent as RelatedContentInterface;
use ChildTheme\SocialSharer\SocialSharerPost;

/**
 * Class RelatedContent
 * @package ChildTheme\RelatedContent
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class RelatedContentPost extends SocialSharerPost implements HasRelatedContent, RelatedContentInterface
{
    const RELATED_TAXONOMY = null;

    /**
     * @param string $context
     * @return array
     */
    public function getAssignedIds(string $context = 'assigned_post_ids'): array
    {
        return $this->field($context) ?: [];
    }
    /**
     * @return array
     */
    public function getRelatedTermIds(): array
    {
        $terms = $this->terms(static::RELATED_TAXONOMY);
        return !empty($terms) ? array_map(function ($term) {
            return $term->term_id;
        }, $terms) : [];
    }
    /**
     * @return string
     */
    public function getRelatedTaxonomyName(): string
    {
        return static::RELATED_TAXONOMY;
    }

    public function getPostId(): int
    {
        return (int) $this->ID;
    }
}
