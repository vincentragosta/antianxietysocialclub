<?php

namespace ChildTheme\Support\Interfaces;

/**
 * Interface HasCategoryLink
 * @package ChildTheme\Support\Interfaces
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
interface HasCategoryLink
{
    function getCategoryLink(): array;
}
