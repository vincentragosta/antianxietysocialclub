<?php

namespace ChildTheme\Support\ValueObjects;

use Backstage\Models\ObjectBase;
use ChildTheme\Topic\TopicRepository;

/**
 * Class SocialCLubTableBanner
 * @package ChildTheme\Support\ValueObjects
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property int $post_count
 * @property int $current_page
 * @property int $global_posts_per_page
 */
class SocialClubTableBanner extends ObjectBase
{
    const DEFAULT_PAGE = 1;
    const LABEL = '<p>Viewing %s topics - %s through %s (of %s total)</p>';

    protected $post_count;
    protected $current_page;
    protected $global_posts_per_page;

    public function __construct($post_count)
    {
        parent::__construct([
            'post_count' => $post_count,
            'current_page' => get_query_var('paged') ?: 1,
            'global_posts_per_page' => get_option('posts_per_page')
        ]);
    }

    public function getCurrentPageWithPostsLowerBound()
    {
        return $this->current_page !== 1 ?
            $this->getCurrentPagedPosts() - $this->global_posts_per_page : static::DEFAULT_PAGE;
    }

    public function getCurrentPageWithPostsUpperBound()
    {
        return $this->post_count < ($result = $this->getCurrentPagedPosts()) ?
            $this->post_count + ($this->current_page - 1) * $this->global_posts_per_page : $result;
    }

    protected function getCurrentPagedPosts()
    {
        return $this->current_page * $this->global_posts_per_page;
    }

    public function __toString()
    {
        return sprintf(
            static::LABEL,
            $this->post_count,
            $this->getCurrentPageWithPostsLowerBound(),
            $this->getCurrentPageWithPostsUpperBound(),
            count((new TopicRepository())->findAll())
        );
    }
}
