<?php

namespace ChildTheme\Handlers;

/**
 * Class CommentFormHandler
 * @package ChildTheme\Handlers
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class CommentFormHandler
{
    public function __construct()
    {
        add_filter('comment_form_default_fields', [$this, 'removeWebsiteField']);
    }

    public function removeWebsiteField($fields)
    {
        if (isset($fields['url'])) {
            unset($fields['url']);
        }
        return $fields;
    }
}
