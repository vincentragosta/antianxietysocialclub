<?php

namespace ChildTheme\Support;

/**
 * Class VcBackgroundColorHandler
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property array $theme_colors
 */
class VcBackgroundColorHandler
{
    protected static $theme_colors = [
        'Gray' => 'gray'
    ];

    public function __construct()
    {
        add_filter('backstage/vc-library/background-colors', [$this, 'addColors']);
    }

    public function addColors($colors)
    {
        return array_merge($colors, static::$theme_colors);
    }
}
