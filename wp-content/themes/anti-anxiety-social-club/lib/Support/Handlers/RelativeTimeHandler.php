<?php

namespace ChildTheme\Support;

/**
 * Class RelativeTimeHandler
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $relative_time
 */
class RelativeTimeHandler
{
    const DEFAULT_LABEL = 'less than 1 second ago';

    protected $relative_time;
    protected static $time_matrix = [
        12 * 30 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second'
    ];

    public function __construct($time)
    {
        $this->relative_time = $this->convertToRelativeTime($time);
    }

    protected function convertToRelativeTime($time)
    {
        $time_difference = time() - $time;
        if ($time_difference < 1) {
            return static::DEFAULT_LABEL;
        }

        $relative_time = '';
        foreach (static::$time_matrix as $seconds => $label) {
            $divisor = $time_difference / $seconds;
            if ($divisor >= 1) {
                $rounded_time = round($divisor);
                $relative_time = sprintf(
                    '%s %s%s ago',
                    $rounded_time,
                    $label,
                    ($rounded_time > 1 ? 's' : '')
                );
                break;
            }
        }
        return $relative_time;
    }

    public function getRelativeTime()
    {
        return $this->relative_time;
    }
}
