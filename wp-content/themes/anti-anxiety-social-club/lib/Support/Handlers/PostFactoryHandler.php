<?php

namespace ChildTheme\Support\Handlers;

use Backstage\Models\PostFactory;
use ChildTheme\Article\Article;
use ChildTheme\Recipe\Recipe;
use ChildTheme\Topic\Topic;

/**
 * Class PostFactoryHandler
 * @package ChildTheme\Support\Handlers
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @versio 1.0
 */
class PostFactoryHandler
{
    protected static $model_classes = [
        Article::class,
        Recipe::class,
        Topic::class
    ];

    public function __construct()
    {
        foreach (static::$model_classes as $class) {
            PostFactory::registerPostModel($class);
        }
    }
}
