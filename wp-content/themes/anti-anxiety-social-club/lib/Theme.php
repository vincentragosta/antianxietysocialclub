<?php

namespace ChildTheme;

use ChildTheme\Article\ArticleController;
use ChildTheme\Components\HomeSlider\HomeSlider;
use ChildTheme\Components\HomeSlider\HomeSliderCard;
use ChildTheme\Components\PostActionCard\PostActionCard;
use ChildTheme\Components\PostActionCardMasonry\PostActionCardMasonry;
use ChildTheme\Handlers\CommentFormHandler;
use ChildTheme\Recipe\RecipeController;
use ChildTheme\Search\SearchController;
use ChildTheme\Support\Handlers\PostFactoryHandler;
use ChildTheme\Support\VcBackgroundColorHandler;
use ChildTheme\Topic\Topic;
use ChildTheme\Topic\TopicController;
use Orchestrator\Theme as ThemeBase;

/**
 * Class Theme
 *
 * Configure settings by overriding parent class constants
 *
 * @package Theme
 */
class Theme extends ThemeBase
{
    const EXTENSIONS = [
        ArticleController::class,
        RecipeController::class,
        SearchController::class,
        TopicController::class,
        PostFactoryHandler::class,
        VcBackgroundColorHandler::class,
        CommentFormHandler::class,
        PostActionCard::class,
        PostActionCardMasonry::class,
        HomeSlider::class,
        HomeSliderCard::class
    ];

    const RENAME_DEFAULT_POST_TYPE = Topic::LABEL;

    const PLATFORM_THEME_SUPPORT = [
        'set-design/nav-menu',
        'design-producer',
        'video-producer',
        'media-gallery-producer',
        'preview-producer',
        'message-producer'
    ];

    /**
     * Add theme-specific style and script enqueues
     */
    public function assets()
    {
        parent::assets();
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Oswald:400,600|Raleway:400,600&display=swap', null, null);

        // consider enqueueing in VC component
        wp_enqueue_script('masonry-js', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', ['jquery'], '1.0', true);
    }
}
