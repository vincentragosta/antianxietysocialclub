<?php

namespace ChildTheme\SocialSharer;

use Backstage\Models\PostBase;

/**
 * Class SocialSharerPost
 * @package ChildTheme\Support\Models
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class SocialSharerPost extends PostBase
{
    /**
     * @return array
     */
    public function getSocialSharer(): array
    {
        $url = $this->permalink();
        return [
            new FacebookSocialSharer($url),
            new TwitterSocialSharer($url, $this->title()),
            new EmailSocialSharer($url, $this->title())
        ];
    }
}
