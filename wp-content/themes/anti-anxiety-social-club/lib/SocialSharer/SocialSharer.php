<?php

namespace ChildTheme\SocialSharer;

use Backstage\SetDesign\Icon\IconView;
use Backstage\View\Link;

/**
 * Class SocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
abstract class SocialSharer
{
    const SHARER = null;
    const ICON = null;

    protected $url;

    public function __construct(string $url)
    {
        $this->url = $this->generateSharerUrl($url);
    }

    public function __toString()
    {
        if (is_null(static::SHARER) || is_null(static::ICON)) {
            return '';
        }
        return (string) new Link($this->url, new IconView(['icon_name' => static::ICON]), ['target' => '_blank']);
    }

    public function getUrl()
    {
        return $this->url;
    }

    abstract protected function generateSharerUrl(string $url, string $text = null);
}
