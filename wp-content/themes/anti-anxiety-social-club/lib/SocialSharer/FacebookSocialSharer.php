<?php

namespace ChildTheme\SocialSharer;

/**
 * Class FacebookSocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class FacebookSocialSharer extends SocialSharer
{
    const SHARER = 'https://www.facebook.com/sharer/sharer.php?u=%s';
    const ICON = 'facebook';

    protected function generateSharerUrl(string $url, string $text = null)
    {
        if (empty($url)) {
            return '';
        }
        return sprintf(static::SHARER, $url);
    }
}
