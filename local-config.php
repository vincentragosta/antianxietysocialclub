<?php
/*
This is a sample local-config.php file
In it, you *must* include the three main database defines

You may include other settings here for various environments
*/

//define( 'DB_NAME', 'scotchbox' );
//define( 'DB_USER', 'root' );
//define( 'DB_PASSWORD', 'root' );

define( 'DB_NAME', 'antianxiety' );
define( 'DB_USER', 'antianxiety' );
define( 'DB_PASSWORD', 'root' );

define( 'WP_SITEURL', 'https://antianxietysocialclub.com/wp' );
define( 'WP_HOME', 'https://antianxietysocialclub.com/' );
define( 'WP_CONTENT_URL', 'https://antianxietysocialclub.com/wp-content' );
define( 'WP_CONTENT_DIR', __DIR__ . '/wp-content' );