<?php

// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
//if (class_exists('\Situation\WPConfig')) {
//    new \Situation\WPConfig(__DIR__);
//}

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         'iBgj,5Pn9/}fxt)G!8F9z_$^fw!|CCfW~t3]z5vxm]%AK>&}0OF![+}+s>:++TYl');
define('SECURE_AUTH_KEY',  'f Yg,W2wT~v5RM`9i>:3A5;_;dMUhL&PI+z+7MfUnM-&o``E&ZLbwA}nLG.f--%:');
define('LOGGED_IN_KEY',    '`,+Bqjf]kqm[PjY*&mRD:Hj$BCs1fK31=C]UaU@42&$[G(,KUr0W6&ZLqh[=^;{X');
define('NONCE_KEY',        ' -B#}|LUW_G~EhX23CMb5K[Xg^06`O(rrMEv$20{,GZQnskzy:-Ip9zReOeD@Z38');
define('AUTH_SALT',        'DKOOn#I?U#u?N:+o.OJt6D++5xqj&Xt8b(QegH[o`RT0|Kkl@^|x1$%=],Ha+0-H');
define('SECURE_AUTH_SALT', 'PW(v)+Q!-5k|Ev]SoGG}&BvUhM[Oa`_KFiquPRHjGwa)_P;>.+B#n77;M%rtGK<|');
define('LOGGED_IN_SALT',   'M}^{)a-m{A1xH-$kG~@R$`[pt&_>hal[I^Ec~&?k-=; ?Lj h_e_<]TrQZ,vhe?~');
define('NONCE_SALT',       'glY4e:74j1y<CsFBQzRUBsYH5v`+ktLI!6+m36&&k|hvSLv?v0V0]?z?$h7Crq_J');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');
